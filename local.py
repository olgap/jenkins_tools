#!/usr/bin/env python

import sys
import Next_Update 
import Utils

utils = Utils.Utils() 


def main():
    args = utils.parse_args()
    if not args.workspace:
        print """You specified no arguments.
                """
        sys.exit()
    upd = Next_Update.Next_Update(args.workspace)
    if args.setup:
#        upd.update_repo('5.1.x')
        upd.setup_repo()
        upd.rebuild_cache()
        upd.install_composer()
    if args.test:
        if not args.setup:
            upd.install_composer()
#        upd.setup_phpvirt_env()
        upd.run_unit_tests()
        upd.run_integration_tests()



if __name__ == '__main__':
    main()
