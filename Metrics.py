#!/usr/bin/env python

import sys
import os
import fnmatch
from multiprocessing import Pool
import Utils
import logging

utils = Utils.Utils()

def _lint(fname):
    cwd = os.getcwd()
    cmd = ['php', '-l', fname]
    res, out = utils.call_silent(cmd, cwd=cwd)
    if res:
        logging.warning("Syntax error found by lint! %s" % out)

class Metrics:

    def __init__(self):
        pass

    def run_lint(self, cwd):
        utils.log(self, "Running lint.")
        ex_dirs = ["var", "classes", "Includes", "lib"]
        ex_files = ["PEAR/Autoloader.php"]
        logging.info("Dirs excluded: %s" % ex_dirs)
        logging.info("Files excluded: %s" % ex_files)
        fnames = []
        for root, dirs, files in os.walk(cwd, topdown=True):
            dirs[:] = [d for d in dirs if d not in ex_dirs]
            for filename in fnmatch.filter(files, '*.php'):
                for name in ex_files:
                    if name not in os.path.join(root, filename):
                        fnames.append(os.path.join(root, filename))
        pool = Pool(processes=3)
        pool.map(_lint, fnames)

    def run_phpcpd(self, cwd, log_dir):
        utils.log(self, "Running copy-paste detector.")
        log_file = os.path.join(log_dir, "pmd_cpd.xml")
        cmd = 'phpcpd --exclude lib --exclude upgrade --log-pmd %s .' % log_file 
        utils.call_silent(cmd.split(), cwd)

    def run_cs(self, app_dir, log_dir):
        utils.log(self, "Running code sniffer.")
        cmds = ['rm -rf next-sdk', 'git clone https://github.com/xcart/next-sdk.git']
        for cmd in cmds:
            utils.call_silent(cmd.split(), app_dir)

        sdk_dir = os.path.join(app_dir, "next-sdk")
        if not os.path.exists(sdk_dir):
            logging.error("next-sdk directory is absent!")
            return 1
        log_file = os.path.join(log_dir, "checkstyle.xml")
        standard = os.path.join(app_dir, 'next-sdk/devkit/codesniffer/sniffs/XLite')
        ignore = [app_dir+x for x in ['/*/upgrade/*', '/*/lib/*']]
        target = os.path.join(app_dir, 'src/classes')
        phpcs = os.path.join(app_dir, 'next-sdk/devkit/codesniffer/phpcs.php')
        cmd = "%s -s -p --report=checkstyle --report-file=%s --standard=%s --ignore=%s %s" % \
             (phpcs, log_file, standard, ','.join(ignore), target)
        utils.call_verbose(cmd.split(), app_dir) 

