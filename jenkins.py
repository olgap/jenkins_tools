#!/usr/bin/env python

import sys
import Next_Update 
import Utils

utils = Utils.Utils() 


def main():
    args = utils.parse_args()
    if not args.workspace:
        print """You specified no arguments.
                """
        sys.exit()
    upd = Next_Update.Next_Update(args.workspace)
    if args.setup:
        upd.setup_repo()
        upd.rebuild_cache()
        upd.install_composer()
    if args.test:
        pass 
    upd.run_metrics(args)


if __name__ == '__main__':
    main()
