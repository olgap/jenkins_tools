#!/usr/bin/env python

import subprocess
import os
import sys
import time
import urllib
import shutil
import logging
import Utils
import Metrics

utils = Utils.Utils()
metrics = Metrics.Metrics()

def _lint(fname):
    cwd = os.getcwd()
    cmd = ['php', '-l', fname]
    utils.call_verbose(cmd, cwd=cwd)

class Next_Update:
    """Update Next and optionally run unit tests

    TBD:
    Support virtphp envs, os.environ or smth
    Allow to switch on/off metrics (requires booleans from jenkins)
    Prepend package name in JUnit reports

    Check dir exists everywhere
    Get rid of spare dir vars
    Add CL args (app_dir = $WORKSPACE)
    Handle return codes where needed
    Assume git -dfx, ensure initial setup (copy config, remove Main.php, etc)
    """

    def __init__(self, app_dir):
        logging.basicConfig(level=logging.INFO)
        self.app_dir = app_dir
        self.home_dir = os.environ['HOME'] 
        self.tools_dir = os.path.join(self.home_dir, 'jenkins_tools')
        self.jenkins = False
        if 'jenkins' in self.app_dir:
            self.jenkins = True 
        self.log_dir = os.path.join(self.app_dir, 'logs')
        if not os.path.exists(self.log_dir):
            os.mkdir(self.log_dir)

    def update_repo(self, branch):
        cmds = [['git', 'checkout', branch], ['git', 'pull']]
        cwd = self.app_dir
        for cmd in cmds:
            utils.log(self, 'Updating repo in %s from branch %s' % (cwd, branch))
            utils.call_verbose(cmd, cwd)

    def setup_repo(self):
        utils.log(self, 'Doing some magic.')
        path1 = os.path.join(self.app_dir, 'src/classes/XLite/Module/CDev/System/Main.php')
        path2 = os.path.join(self.app_dir, 'src/classes/XLite/Module/CDev/System/_Main.php')
        if os.path.exists(path1):
            print "Removing Main.php."
            shutil.move(path1, path2)
        else:
            print "OK, Main.php not found."
#        path1 = os.path.join(self.tools_dir, 'next_config.php')
        path2 = os.path.join(self.app_dir, 'src/etc/config.php')
        if not os.path.exists(path2) and not self.jenkins:
            logging.error("src/etc/config.php is missing");
            sys.exit(1);
#            print "Restoring config.php from my copy."
#            shutil.copy(path1, path2)
#        else:
#            print "OK, config.php exists."

            
    def install_composer(self):
        utils.log(self, 'Install composer and vendor software.')
        cwd = os.path.join(self.app_dir, '.dev/tests') 
        url = 'https://getcomposer.org/installer'
        try:
            urllib.urlretrieve(url, os.path.join(cwd, "installer"))
        except Exception, exc:
            sys.exit("Unable to download composer installer. Check internet connection.")
        cmd1 = 'php installer'
        utils.call_verbose(cmd1.split(), cwd)
        cmd2 = 'php composer.phar install'
        utils.call_verbose(cmd2.split(), cwd)
        cmd3 = 'php composer.phar update'
        utils.call_verbose(cmd3.split(), cwd)


    def _setup_phpvirt_env(self):
        utils.log(self, 'FAKE: Setup phpvirt environment and run unit tests.')
        cwd = os.path.join(self.app_dir, '.dev/tests') 
        php_dir = os.path.join(self.home_dir, '.phpenv/versions/')
        if self.jenkins:
            envs = {'php55-env': '5.5.9', 'php54-env': '5.4.26', 'php53-env': '5.3.28'}
        else:
            envs = {'php55-env': '5.5.9'}
        for env in envs:
            if os.path.exists(os.path.join(self.app_dir, '.dev/tests', env)): 
                shutil.rmtree(os.path.join(self.app_dir, '.dev/tests', env))
            php_bin_dir = os.path.join(php_dir, envs[env], 'bin')
            cmd1 = 'virtphp create --php-bin-dir=%s %s' % (php_bin_dir, env)
            utils.call_verbose(cmd1.split(), cwd)
            cmd2 = os.path.join(self.app_dir, '.dev/tests', env, 'bin/activate')
            execfile(cmd2)
            

    def run_metrics(self, args):
        if args.lint == 'true':
            metrics.run_lint(os.path.join(self.app_dir, 'src'))
        if args.phpcpd == 'true':
            metrics.run_phpcpd(os.path.join(self.app_dir, 'src/classes'), self.log_dir)
        if args.cs == 'true':
            metrics.run_cs(self.app_dir, self.log_dir)


    def run_unit_tests(self):
        utils.log(self, 'Run unit tests.')
        cwd = os.path.join(self.app_dir, '.dev/tests')
        cmd = './vendor/phpunit/phpunit/phpunit -c ./phpunit.xml --testsuite Unit --log-junit ./unit.xml'
        utils.call_verbose(cmd.split(), cwd=cwd)

    def run_integration_tests(self):
        utils.log(self, 'Run integration tests.')
        cwd = os.path.join(self.app_dir, '.dev/tests')
        cmd = './vendor/phpunit/phpunit/phpunit -c ./phpunit.xml --testsuite Integration --log-junit ./integration.xml'
        utils.call_verbose(cmd.split(), cwd=cwd)

    def edit_test_report(self):
        pass

    def rebuild_cache(self):
        utils.log(self, 'Rebuild cache.')
        cwd = os.path.join(self.app_dir, 'src')
        cmd1 = './restoredb standalone admin'
        utils.call_verbose(cmd1.split(), cwd=cwd)
        cmd2 = 'php ./admin.php'
        errors = ('PHP Fatal error', 'ERROR')
        while True:
            print 'Rebuilding...'
            res,out = utils.call_verbose(cmd2.split(), cwd=cwd)
            if 'Deploying store' not in out:
                break
            if res or any(error in out for error in errors):
                logging.error("Errors during rebuild. Try rerunning the build.")
                sys.exit(1)

                

def main():
    print "Use local.py or jenkins.py to run."


if __name__ == '__main__':
    main()
