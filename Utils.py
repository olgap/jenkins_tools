#!/usr/bin/env python

import subprocess
import datetime
import sys
import os
import argparse

class Utils:

    def __init__(self):
        pass

    def call_silent(self, cmd, cwd=None):
        process = subprocess.Popen(cmd, cwd=cwd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        out, err = process.communicate()
        try:
            res = process.wait()
        except Exception, exc:
            os.kill(popen.pid, signal.SIGINT)
            res = 1
        return (res, out)

    def call_verbose(self, cmd, cwd=None):
        sys.stdout.write("======== Calling: %s ========\nPath: %s\nCommand: %s\n" %
                (str(datetime.datetime.now()), cwd, " ".join(cmd)))
        res, out = self.call_silent(cmd, cwd=cwd)
        print out
        return (res, out)

    def _call_verbose(self, cmd, cwd=None):
        sys.stdout.write("======== Calling: %s ========\nPath: %s\nCommand: %s\n" %
            (str(datetime.datetime.now()), cwd, " ".join(cmd)))
        process = subprocess.Popen(cmd, cwd=cwd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        out, err = process.communicate()
        print out
        try:
            res = process.wait()
        except Exception, exc:
            os.kill(popen.pid, signal.SIGINT)
            res = 1 
        return (res, out)

    def log(self, module, msg):
        date = str(datetime.datetime.now())
        sys.stdout.write(" ==== %s %s ====\n%s\n" % (module.__class__.__name__, date, msg))
        sys.stdout.flush()

    def parse_args(self):
        parser = argparse.ArgumentParser()
        parser.add_argument("--workspace", help="Path to workspace directory")
        parser.add_argument("--setup", help="Setup workspace", action="store_true")
        parser.add_argument("--test", help="Run tests", action="store_true")
        metrics = parser.add_argument_group('metrics', 'Code metrics.')
        metrics.add_argument("--lint", help="Run LINT.", choices=['true', 'false'])
        metrics.add_argument("--phpcpd", help="Run PHPCPD.", choices=['true', 'false'])
        metrics.add_argument("--cs", help="Run codesniffer.", choices=['true', 'false'])
        args = parser.parse_args()
        return args



